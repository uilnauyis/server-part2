﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Server
{
    public class ServerController : ApiController
    {
        private const string SERVER_STORAGE_PATH = "files";
        public static HashSet<string> _cachedBlockHash = new HashSet<string>();

		int MAX_BLOCK_SIZE = 10000;
		int MIN_BLOCK_SIZE = 100;

        static ServerController()
        {
            
        }

        [HttpGet]
        public async Task<IHttpActionResult> ListFiles()
        {
            List<string> fileNames = new List<string>();
            await Task.Run(() =>
            {
                fileNames = readFiles();
            });
            return Ok(fileNames);
        }

        [HttpGet]
        public async Task<IHttpActionResult> DownloadFile([FromUri]string fileName = null)
        {
            var availableFiles = readFiles();
            if (!availableFiles.Contains(fileName))
            {
                return NotFound();
            }

            try
            {
                MemoryStream responseStream = new MemoryStream();
                byte[] bytes = File.ReadAllBytes($"{SERVER_STORAGE_PATH}/{fileName}");
                List<byte[]> result = SplitFileContentIntoBlocks(bytes);
                await Digest(result, fileName);

                return Ok(result);
            }
            catch (IOException exception)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                    "Exception happens while attempting to process the file"));
            }
        }

        [HttpGet]
        public async Task ClearCache()
        {
            await Task.Run(() =>
            {
                _cachedBlockHash.Clear();
            });
        }

        private List<string> readFiles()
        {
            var files = new List<string>();
            foreach (string file in Directory.EnumerateFiles(SERVER_STORAGE_PATH))
            {
                files.Add(Path.GetFileName(file));
            }
            return files;
        }

        private List<byte[]> SplitFileContentIntoBlocks(byte[] content)
        {
            List<byte[]> result = new List<byte[]>();


            if (content.Length <= 3)
            {
                result.Add(content);
                return result;
            }

            int lastBoundary = -1;
            
            for (int i = 2; i < content.Length; i++)
            {
                int blockLength = i - lastBoundary;
                if (Isboundary(lastBoundary, i, content) ||
                    i == content.Length - 1 ||
					i - lastBoundary >= MAX_BLOCK_SIZE
                    ) {
                    byte[] block = new byte[blockLength];
                    Array.Copy(content, lastBoundary + 1, block, 0, blockLength);
                    result.Add(block);
                    lastBoundary = i;
                }
            }
            return result;
        }
        
        private bool Isboundary(int lastBoundary, int index, byte[] content)
        {
            const int APPROXIMATE_BLOCK_SIZE =2000;
            const int P = 3;

            var fValue = content[index - 2] * P * P + content[index - 1] * P + content[index];
            return fValue % APPROXIMATE_BLOCK_SIZE == 0
				&& index - lastBoundary >= MIN_BLOCK_SIZE;
        }

        private async Task Digest(List<byte[]> blocks, string fileName)
        {
            await Task.Run(async () =>
            {
                MD5 hashAlgorithm = MD5.Create();
                for (int i = 0; i < blocks.Count; i++)
                {
                    byte[] hash = hashAlgorithm.ComputeHash(blocks[i]);
                    string stringifiedHash = BytesToHexString(hash);
                    if (_cachedBlockHash.Contains(stringifiedHash))
                    {
                        blocks[i] = hash;
                        continue;
                    }
                    _cachedBlockHash.Add(stringifiedHash);
                }
            });
        }

        private string BytesToHexString(byte[] byteArr)
        {
            StringBuilder hex = new StringBuilder(byteArr.Length * 2);
            foreach (byte b in byteArr)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
    }
}
