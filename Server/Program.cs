﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        static AutoResetEvent autoEvent = new AutoResetEvent(false);

        static void Main()
        {
            const string SERVER_STORAGE_PATH = "files";
            string baseAddress = "http://localhost:9000/";

            // Start OWIN host 
            using (WebApp.Start<StartUp>(url: baseAddress))
            {
                Directory.CreateDirectory(SERVER_STORAGE_PATH);
                DirectoryInfo serverLocation = new DirectoryInfo(SERVER_STORAGE_PATH);
                foreach (var file in serverLocation.EnumerateFiles())
                {
                    file.Delete();
                }

                Process.Start($"{SERVER_STORAGE_PATH}");

                Thread.Sleep(Timeout.Infinite);
            }
        }
    }
}
